const gulp = require("gulp");
const mkdirp = require("mkdirp"); //这里要依赖mkdirp这个包，通过cnpm 安装
//ejs模板
const ejs = require("gulp-ejs");
//重命名
const rename = require("gulp-rename");
//游览器
const browserSync = require("browser-sync").create();
//less
const less = require("gulp-less");
const auto = require("gulp-autoprefixer");
const libs = require("./lib/library");

const reload = browserSync.reload;

const dirs = {
	src: "./src",
	css: "./src/css",
	less: "./src/less",
	js: "./src/js",
	img: "./src/img",
};

//创建目录
gulp.task("createDirectory", function (done) {
	for (let i in dirs) {
		mkdirp(dirs[i], err => {
			if (err) {
				console.warn(err);
			}
		});
	}
	done();
});

//HTML主文件处理
gulp.task("mainHTML", function () {
	return gulp.src("./public/index.ejs")
		.pipe(ejs({
			libs: libs
		}))
		.pipe(rename("index.html"))
		.pipe(gulp.dest("./src"));
});

//JS主文件处理
gulp.task("mainJS", function () {
	return gulp.src("./public/index.js")
		.pipe(gulp.dest("./src/js"));
});
//LESS主文件处理
gulp.task("mainCSS", function () {
	return gulp.src("./public/index.less")
		.pipe(less())
		.pipe(auto({
			grid: true,
			browsers: ["last 2 version"]
		}))
		.pipe(gulp.dest("./src/css"))
		.pipe(reload({stream: true}));
});

//子级HTML文件处理
gulp.task("subHTML", function () {
	return gulp.src("./component/*/*/template/*.html")
		.pipe(gulp.dest("./src/template"));
});

//子级JS文件处理
gulp.task("subJS", function () {
	return gulp.src("./component/*/*/*/*.js")
		.pipe(gulp.dest("./src/template"));
});

//子级CSS文件处理
gulp.task("subCSS", function () {
	return gulp.src("./component/*/*/less/*.less")
		.pipe(less())
		.pipe(auto({
			grid: true,
			browsers: ["last 2 version"]
		}))
		.pipe(gulp.dest("./src/template"))
		.pipe(reload({stream: true}));
});
//游览器运行
gulp.task("browserSync", function (done) {
	browserSync.init({
		server: {
			baseDir: "./src"
		}
	});
	done();
});
//
// gulp.watch("./public/index.js", gulp.series("mainJS", function (done) {
// 	browserSync.reload;
// 	done();
// }));

gulp.task("dev-default", gulp.series("createDirectory", gulp.parallel("mainHTML", "mainJS", "mainCSS", "subHTML", "subJS", "subCSS"), "browserSync", function (done) {
	gulp.watch("./public/index.less", gulp.series("mainCSS", function (done) {
		done();
	}));
	gulp.watch("./public/index.ejs", gulp.series("mainHTML", function (done) {
		reload();
		done();
	}));
	gulp.watch("./public/index.js", gulp.series("mainJS", function (done) {
		reload();
		done();
	}));
	gulp.watch("./component/*/*/template/*.html", gulp.series("subHTML", function (done) {
		reload();
		done();
	}));
	gulp.watch("./component/*/*/less/*.less", gulp.series("subCSS", function (done) {
		done();
	}));
	gulp.watch("./component/*/*/*/*.js", gulp.series("subJS", function (done) {
		reload();
		done();
	}));
	done();
}));