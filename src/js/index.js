"use strict";
// eslint-disable-next-line
var app = angular.module("myApp", ["ui.router", "oc.lazyLoad"]);
app.config(["$stateProvider",function ($stateProvider) {
	$stateProvider
		.state("home", {
			url: "/home",
			views:{
				"left":{
					templateUrl: "/template/main/menu/template/menu.html",
				},
				"right":{
					templateUrl: "/template/home/home/template/home.html",
				}
			},
			resolve: {
				homeCtrl: function ($ocLazyLoad) {
					return $ocLazyLoad.load({
						name: "home",
						files: ["/template/main/menu/less/menu.css"]
					});
				}
			}
		})
		.state("menulist", {
			url: "/menuSet/menuList",
			views:{
				"left":{
					templateUrl: "/template/main/menu/template/menu.html",
				},
				"right":{
					templateUrl: "/template/menu/menu/template/menuList.html",
				}
			},
			resolve: {
				menusetCtrl: function ($ocLazyLoad) {
					return $ocLazyLoad.load({
						name: "menulist",
						files: ["/template/main/menu/less/menu.css","/template/menu/menu/controllers/menuList.js"]
					});
				}
			}
		});
}]);
app.controller("mainController", ["$scope", function ($scope) {
	$scope.lift = "信息";
}]);

app.run(["$state",function ($state) {
	$state.go("home");
}]);